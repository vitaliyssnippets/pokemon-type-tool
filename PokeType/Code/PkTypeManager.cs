﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokeType
{
    public class PkTypeManager
    {
        private PkType[] types;

        public PkTypeManager()
        {
            types = new PkType[19];
            LoadAllTypes();
        }

        private void LoadAllTypes()
        {
            int i = 0;
            foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
            {
                types[i] = new PkType(t);
                i++;
            }
        }

        private int TypeToInt(PkType.PokemonType type)
        {
            int i = 0;
            foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
            {
                if(type == t) break;
                i++;
            }
            return i;
        }

        public PkType GetPkType(PkType.PokemonType type)
        {
            return types[TypeToInt(type)];
        }

        public List<PkType.PokemonType> GetDmg200Against(PkType.PokemonType type)
        {
            return GetPkType(type).damage_200;
        }

        public List<PkType.PokemonType> GetDmg0Against(PkType.PokemonType type)
        {
            return GetPkType(type).damage_0;
        }

        public List<PkType.PokemonType> GetDmg100Against(PkType.PokemonType type)
        {
            return GetPkType(type).damage_100;
        }

        public List<PkType.PokemonType> GetDmg50Against(PkType.PokemonType type)
        {
            return GetPkType(type).damage_50;
        }
    }
}
