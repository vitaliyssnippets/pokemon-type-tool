﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokeType
{
    public class PkType
    {
        public enum PokemonType {NORMAL, FIRE, WATER, ELECTRIC, GRASS, ICE, FIGHTING, POISON, GROUND, FLYING, PSYCHIC, BUG,
        ROCK, GHOST, DRAGON, DARK, STEEL, FAIRY};

        public const PokemonType NORMAL = PokemonType.NORMAL;
        public const PokemonType FIRE = PokemonType.FIRE;
        public const PokemonType WATER = PokemonType.WATER;
        public const PokemonType ELECTRIC = PokemonType.ELECTRIC;
        public const PokemonType GRASS = PokemonType.GRASS;
        public const PokemonType ICE = PokemonType.ICE;
        public const PokemonType FIGHTING = PokemonType.FIGHTING;
        public const PokemonType POISON = PokemonType.POISON;
        public const PokemonType GROUND = PokemonType.GROUND;
        public const PokemonType FLYING = PokemonType.FLYING;
        public const PokemonType PSYCHIC = PokemonType.PSYCHIC;
        public const PokemonType BUG = PokemonType.BUG;
        public const PokemonType ROCK = PokemonType.ROCK;
        public const PokemonType GHOST = PokemonType.GHOST;
        public const PokemonType DRAGON = PokemonType.DRAGON;
        public const PokemonType DARK = PokemonType.DARK;
        public const PokemonType STEEL = PokemonType.STEEL;
        public const PokemonType FAIRY = PokemonType.FAIRY;

        private PokemonType type;
        public List<PokemonType> damage_0, damage_50, damage_100, damage_200;
        
        public PkType(PokemonType type)
        {
            this.type = type;
            UpdateLists();
        }

        private void UpdateLists()
        {
            damage_0 = new List<PokemonType>();
            damage_50 = new List<PokemonType>();
            damage_100 = new List<PokemonType>();
            damage_200 = new List<PokemonType>();
            damage_0.Clear();
            damage_50.Clear();
            damage_100.Clear();
            damage_200.Clear();
            LoadLists();
        }

        private void LoadLists()
        {
            LoadListNormal();
            LoadListFire();
            LoadListWater();
            LoadListElectric();
            LoadListGrass();
            LoadListIce();
            LoadListFighting();
            LoadListPoison();
            LoadListGround();
            LoadListFlying();
            LoadListPsychic();
            LoadListBug();
            LoadListRock();
            LoadListGhost();
            LoadListDragon();
            LoadListDark();
            LoadListSteel();
            LoadListFairy();
            LoadDamage_100();
        }

        private void LoadListFairy()
        {
            if (typeIs(FAIRY))
            {
                damage_50.Add(FIRE);
                damage_50.Add(POISON);
                damage_50.Add(STEEL);

                damage_200.Add(FIGHTING);
                damage_200.Add(DRAGON);
                damage_200.Add(DARK);
            }
        }

        private void LoadListSteel()
        {
            if (typeIs(STEEL))
            {
                damage_50.Add(FIRE);
                damage_50.Add(WATER);
                damage_50.Add(ELECTRIC);
                damage_50.Add(STEEL);

                damage_200.Add(ICE);
                damage_200.Add(ROCK);
                damage_200.Add(FAIRY);
            }
        }

        private void LoadListDark()
        {
            if (typeIs(DARK))
            {
                damage_50.Add(FIGHTING);
                damage_50.Add(DARK);
                damage_50.Add(FAIRY);

                damage_200.Add(PSYCHIC);
                damage_200.Add(GHOST);
            }
        }

        private void LoadListDragon()
        {
            if (typeIs(DRAGON))
            {
                damage_0.Add(FAIRY);

                damage_50.Add(STEEL);

                damage_200.Add(DRAGON);
            }
        }

        private void LoadListGhost()
        {
            if (typeIs(GHOST))
            {
                damage_0.Add(NORMAL);
                
                damage_50.Add(GHOST);

                damage_200.Add(PSYCHIC);
                damage_200.Add(GHOST);
            }
        }

        private void LoadListRock()
        {
            if (typeIs(ROCK))
            {
                damage_50.Add(FIGHTING);
                damage_50.Add(GROUND);
                damage_50.Add(STEEL);

                damage_200.Add(FIRE);
                damage_200.Add(ICE);
                damage_200.Add(FLYING);
                damage_200.Add(BUG);
            }
        }

        private void LoadListBug()
        {
            if (typeIs(BUG))
            {
                damage_50.Add(FIRE);
                damage_50.Add(FIGHTING);
                damage_50.Add(POISON);
                damage_50.Add(FLYING);
                damage_50.Add(GHOST);
                damage_50.Add(STEEL);
                damage_50.Add(FAIRY);

                damage_200.Add(GRASS);
                damage_200.Add(PSYCHIC);
                damage_200.Add(DARK);
            }
        }

        private void LoadListPsychic()
        {
            if (typeIs(PSYCHIC))
            {
                damage_0.Add(DARK);

                damage_50.Add(PSYCHIC);
                damage_50.Add(STEEL);

                damage_200.Add(FIGHTING);
                damage_200.Add(POISON);
            }
        }

        private void LoadListFlying()
        {
            if (typeIs(FLYING))
            {
                damage_50.Add(ELECTRIC);
                damage_50.Add(ROCK);
                damage_50.Add(STEEL);

                damage_200.Add(GRASS);
                damage_200.Add(FIGHTING);
                damage_200.Add(BUG);
            }
        }

        private void LoadListGround()
        {
            if (typeIs(GROUND))
            {
                damage_0.Add(FLYING);

                damage_50.Add(GRASS);
                damage_50.Add(BUG);

                damage_200.Add(FIRE);
                damage_200.Add(ELECTRIC);
                damage_200.Add(POISON);
                damage_200.Add(ROCK);
                damage_200.Add(STEEL);
            }
        }

        private void LoadListPoison()
        {
            if (typeIs(POISON))
            {
                damage_0.Add(STEEL);

                damage_50.Add(POISON);
                damage_50.Add(GROUND);
                damage_50.Add(ROCK);
                damage_50.Add(GHOST);

                damage_200.Add(GRASS);
                damage_200.Add(FAIRY);
            }
        }

        private void LoadListFighting()
        {
            if (typeIs(FIGHTING))
            {
                damage_0.Add(GHOST);

                damage_50.Add(POISON);
                damage_50.Add(GROUND);
                damage_50.Add(FLYING);
                damage_50.Add(PSYCHIC);
                damage_50.Add(BUG);
                damage_50.Add(FAIRY);

                damage_200.Add(NORMAL);
                damage_200.Add(ICE);
                damage_200.Add(ROCK);
                damage_200.Add(DARK);
                damage_200.Add(STEEL);
            }
        }

        private void LoadListIce()
        {
            if (typeIs(ICE))
            {
                damage_50.Add(FIRE);
                damage_50.Add(WATER);
                damage_50.Add(ICE);
                damage_50.Add(STEEL);

                damage_200.Add(GRASS);
                damage_200.Add(GROUND);
                damage_200.Add(FLYING);
                damage_200.Add(DRAGON);           
            }
        }

        private void LoadListGrass()
        {
            if (typeIs(GRASS))
            {
                damage_50.Add(FIRE);
                damage_50.Add(GRASS);
                damage_50.Add(POISON);
                damage_50.Add(FLYING);
                damage_50.Add(BUG);
                damage_50.Add(DRAGON);
                damage_50.Add(STEEL);

                damage_200.Add(WATER);
                damage_200.Add(GROUND);
                damage_200.Add(ROCK);
            }
        }

        private void LoadListElectric()
        {
            if (typeIs(ELECTRIC))
            {
                damage_0.Add(GROUND);

                damage_50.Add(ELECTRIC);
                damage_50.Add(GRASS);
                damage_50.Add(DRAGON);

                damage_200.Add(WATER);
                damage_200.Add(FLYING);
            }
        }

        private void LoadListWater()
        {
            if (typeIs(WATER))
            {
                damage_50.Add(WATER);
                damage_50.Add(GRASS);
                damage_50.Add(DRAGON);

                damage_200.Add(FIRE);
                damage_200.Add(GROUND);
                damage_200.Add(ROCK);
            }
        }

        private void LoadListFire()
        {
            if (typeIs(FIRE))
            {
                damage_50.Add(FIRE);
                damage_50.Add(WATER);
                damage_50.Add(ROCK);
                damage_50.Add(DRAGON);

                damage_200.Add(GRASS);
                damage_200.Add(ICE);
                damage_200.Add(BUG);
                damage_200.Add(STEEL);
            }
        }

        private void LoadListNormal()
        {
            if (typeIs(NORMAL))
            {
                damage_0.Add(GHOST);
                damage_50.Add(ROCK);
                damage_50.Add(STEEL);
            }
        }

        private void LoadDamage_100()
        {
            foreach (PokemonType t in Enum.GetValues(typeof(PokemonType)))
            {
                if (!damage_0.Contains(t) && !damage_50.Contains(t) && !damage_200.Contains(t))
                    damage_100.Add(t);
            }
        }

        public bool typeIs(PokemonType type)
        {
            return this.type == type;
        }
    }
}
