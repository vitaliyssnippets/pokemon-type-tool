﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokeType
{
    public partial class frmMain : Form
    {

        PkTypeManager manager;
        String selected1, selected2;
        byte selection;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            manager = new PkTypeManager();
            selected1 = null;
            selected2 = null;
            lblResults.Text = "";
            lblOffences.Text = lblOffences2.Text = "";
            selection = 0;
            int i = 0;
            foreach (TabPage tab in tabController.Controls)
            {
                if (i == 0)
                {
                    foreach (Button button in tab.Controls)//Loop over controls in the tab (buttons)
                    {
                        foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
                        {
                            if (button.Name.ToUpper().Contains(t.ToString()))
                            {
                                button.Click += new System.EventHandler(btnType_Click);//Implement an event handler for each button
                                button.ForeColor = Color.Black;
                                button.Font = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Bold);
                            }
                        }
                    }
                    i++;
                }
            }
        }

        private void btnType_Click(object sender, EventArgs e)
        {
            if (selection == 0)
            {
                selected1 = ((Control)sender).Text.ToUpper();
                selected2 = null;
                selection = 1;
                lblSelected.Text = selected1;
            } else if (selection == 1)
            {
                selected2 = ((Control)sender).Text.ToUpper();
                if(selected2.Equals(selected1))
                {
                    selected1 = null;
                    selected2 = null;
                    selection = 0;
                    lblSelected.Text = "PLEASE CHOOSE 1-2 TYPES";
                }
                else 
                {
                    selection = 2;
                    lblSelected.Text = selected1 + " - " + selected2;
                }
            }
            else
            {
                selected1 = null;
                selected2 = null;
                selection = 0;
                lblSelected.Text = "PLEASE CHOOSE 1-2 TYPES";
            }
            updateResults();
        }

        private void solveDefences()
        {
            List<String> lst = new List<String>();
            List<String> fixedList = new List<String>();
            PkType.PokemonType selected1Type = PkType.NORMAL;
            PkType.PokemonType selected2Type = PkType.NORMAL;
            foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
            {
                if (t.ToString().Equals(selected1))
                {
                    selected1Type = t;
                }
                if (t.ToString().Equals(selected2))
                {
                    selected2Type = t;
                }
            }

            if (selected1 != null && selected2 == null)
            {
                lst.Add("\nTypes 2x against " + selected1 + ":\n");
                foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
                {
                    if (manager.GetPkType(t).damage_200.Contains(selected1Type))
                    {
                        lst.Add(t.ToString() + "\n");
                    }
                }
                lst.Add("\nTypes 0.5x against " + selected1 + ":\n");
                foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
                {
                    if (manager.GetPkType(t).damage_50.Contains(selected1Type))
                    {
                        lst.Add(t.ToString() + "\n");
                    }
                }
                lst.Add("\nTypes 0x against " + selected1 + ":\n");
                foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
                {
                    if (manager.GetPkType(t).damage_0.Contains(selected1Type))
                    {
                        lst.Add(t.ToString() + "\n");
                    }
                }
                lst.Add("\nTypes 1x against " + selected1 + ":\n");
                foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
                {
                    if (manager.GetPkType(t).damage_100.Contains(selected1Type))
                    {
                        lst.Add(t.ToString() + "\n");
                    }
                }
                fixedList = lst;
            }

            if (selected1 != null && selected2 != null)
            {
                foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
                {
                    double mult = 1.0;
                    if (manager.GetPkType(t).damage_200.Contains(selected1Type))
                    {
                        mult = mult * 2.0;
                    }
                    if (manager.GetPkType(t).damage_200.Contains(selected2Type))
                    {
                        mult = mult * 2.0;
                    }
                    if (manager.GetPkType(t).damage_50.Contains(selected1Type))
                    {
                        mult = mult * 0.5;
                    }
                    if (manager.GetPkType(t).damage_50.Contains(selected2Type))
                    {
                        mult = mult * 0.5;
                    }
                    if (manager.GetPkType(t).damage_0.Contains(selected1Type) || manager.GetPkType(t).damage_0.Contains(selected2Type))
                    {
                        mult = 0;
                    }

                    lst.Add("[" + mult + "x] " + t.ToString() + "\n");
                    lst.Sort(CompareEffectiveness);
                }

                fixedList.Add("\nTypes 4x against " + selected1 + " - " + selected2 + ":\n");
                foreach (String s in lst)
                {
                    if (s.Contains("4"))
                    {
                        int k = 0;
                        while (s[k] != ' ') k++;
                        k++;
                        fixedList.Add(s.Substring(k));
                    }
                }
                fixedList.Add("\nTypes 2x against " + selected1 + " - " + selected2 + ":\n");
                foreach (String s in lst)
                {
                    if (s.Contains("2"))
                    {
                        int k = 0;
                        while (s[k] != ' ') k++;
                        k++;
                        fixedList.Add(s.Substring(k));
                    }
                }
                fixedList.Add("\nTypes 0.5x against " + selected1 + " - " + selected2 + ":\n");
                foreach (String s in lst)
                {
                    if (s.Contains(".5"))
                    {
                        int k = 0;
                        while (s[k] != ' ') k++;
                        k++;
                        fixedList.Add(s.Substring(k));
                    }
                }
                fixedList.Add("\nTypes 0.25x against " + selected1 + " - " + selected2 + ":\n");
                foreach (String s in lst)
                {
                    if (s.Contains(".2"))
                    {
                        int k = 0;
                        while (s[k] != ' ') k++;
                        k++;
                        fixedList.Add(s.Substring(k));
                    }
                }
                fixedList.Add("\nTypes 1x against " + selected1 + " - " + selected2 + ":\n");
                foreach (String s in lst)
                {
                    if (s.Contains("1"))
                    {
                        int k = 0;
                        while (s[k] != ' ') k++;
                        k++;
                        fixedList.Add(s.Substring(k));
                    }
                }
            }

            fixedList = cleanList(fixedList);

            lblResults.Text = listToString(fixedList);
        }

        private List<String> cleanList(List<String> list)
        {
            String previous = "";
            List<String> copy = new List<String>();
            foreach (String str in list)
                copy.Add(str);
            foreach (String str in list)
            {
                if (str.Contains("against") && previous.Contains("against"))
                {
                    copy.Remove(previous);
                }
                previous = str;
            }
            return copy;
        }

        public void solveOffences1()
        {
            List<String> finalList = new List<String>();
            PkType.PokemonType selected1Type = PkType.NORMAL;
            PkType.PokemonType selected2Type = PkType.NORMAL;
            foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
            {
                if (t.ToString().Equals(selected1))
                {
                    selected1Type = t;
                }
                if (t.ToString().Equals(selected2))
                {
                    selected2Type = t;
                }
            }

            finalList.Add("\n" + selected1 + " is 2x against:\n");
            foreach(PkType.PokemonType type in manager.GetPkType(selected1Type).damage_200)
            {
                finalList.Add(type.ToString() + "\n");
            }

            finalList.Add("\n" + selected1 + " is 0.5x against:\n");
            foreach (PkType.PokemonType type in manager.GetPkType(selected1Type).damage_50)
            {
                finalList.Add(type.ToString() + "\n");
            }

            finalList.Add("\n" + selected1 + " is 0x against:\n");
            foreach (PkType.PokemonType type in manager.GetPkType(selected1Type).damage_0)
            {
                finalList.Add(type.ToString() + "\n");
            }

            finalList.Add("\n" + selected1 + " is 1x against:\n");
            foreach (PkType.PokemonType type in manager.GetPkType(selected1Type).damage_100)
            {
                finalList.Add(type.ToString() + "\n");
            }

            finalList = cleanList(finalList);

            lblOffences.Text = listToString(finalList);
        }

        public void solveOffences2()
        {
            if (selected2 != null)
            {
                List<String> finalList = new List<String>();
                PkType.PokemonType selected1Type = PkType.NORMAL;
                PkType.PokemonType selected2Type = PkType.NORMAL;
                foreach (PkType.PokemonType t in Enum.GetValues(typeof(PkType.PokemonType)))
                {
                    if (t.ToString().Equals(selected1))
                    {
                        selected1Type = t;
                    }
                    if (t.ToString().Equals(selected2))
                    {
                        selected2Type = t;
                    }
                }

                finalList.Add("\n" + selected2 + " is 2x against:\n");
                foreach (PkType.PokemonType type in manager.GetPkType(selected2Type).damage_200)
                {
                    finalList.Add(type.ToString() + "\n");
                }

                finalList.Add("\n" + selected2 + " is 0.5x against:\n");
                foreach (PkType.PokemonType type in manager.GetPkType(selected2Type).damage_50)
                {
                    finalList.Add(type.ToString() + "\n");
                }

                finalList.Add("\n" + selected2 + " is 0x against:\n");
                foreach (PkType.PokemonType type in manager.GetPkType(selected2Type).damage_0)
                {
                    finalList.Add(type.ToString() + "\n");
                }

                finalList.Add("\n" + selected2 + " is 1x against:\n");
                foreach (PkType.PokemonType type in manager.GetPkType(selected2Type).damage_100)
                {
                    finalList.Add(type.ToString() + "\n");
                }

                finalList = cleanList(finalList);

                lblOffences2.Text = listToString(finalList);
            }
        }

        public void updateResults()
        {
            solveDefences();
            solveOffences1();
            solveOffences2();
        }

        private static int CompareEffectiveness(String first, String second)
        {
            int i = 0;
            int j = 0;
            while (first[i] != 'x') i++;
            while (second[j] != 'x') j++;
            double f = Convert.ToDouble(first.Substring(1, i - 1));
            double s = Convert.ToDouble(second.Substring(1, j - 1));
            if (f == s) return 0;
            if (f < s) return 1;
            if (f > s) return -1;
            return 0;
        }

        private String listToString(List<String> list)
        {
            String temp = "";
            foreach(String s in list)
            {
                temp += s;
            }
            return temp;
        }

    }
}
