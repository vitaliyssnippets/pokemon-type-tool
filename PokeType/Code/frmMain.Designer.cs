﻿namespace PokeType
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabController = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnElectric = new System.Windows.Forms.Button();
            this.btnPsychic = new System.Windows.Forms.Button();
            this.btnFlying = new System.Windows.Forms.Button();
            this.btnPoison = new System.Windows.Forms.Button();
            this.btnBug = new System.Windows.Forms.Button();
            this.btnDark = new System.Windows.Forms.Button();
            this.btnIce = new System.Windows.Forms.Button();
            this.btnFairy = new System.Windows.Forms.Button();
            this.btnDragon = new System.Windows.Forms.Button();
            this.btnGhost = new System.Windows.Forms.Button();
            this.btnFighting = new System.Windows.Forms.Button();
            this.btnNormal = new System.Windows.Forms.Button();
            this.btnSteel = new System.Windows.Forms.Button();
            this.btnGround = new System.Windows.Forms.Button();
            this.btnRock = new System.Windows.Forms.Button();
            this.btnGrass = new System.Windows.Forms.Button();
            this.btnWater = new System.Windows.Forms.Button();
            this.btnFire = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblResults = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lblOffences2 = new System.Windows.Forms.Label();
            this.lblOffences = new System.Windows.Forms.Label();
            this.lblSelected = new System.Windows.Forms.Label();
            this.tabController.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabController
            // 
            this.tabController.Controls.Add(this.tabPage1);
            this.tabController.Controls.Add(this.tabPage2);
            this.tabController.Controls.Add(this.tabPage3);
            this.tabController.Location = new System.Drawing.Point(0, 0);
            this.tabController.Name = "tabController";
            this.tabController.SelectedIndex = 0;
            this.tabController.Size = new System.Drawing.Size(414, 590);
            this.tabController.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.btnElectric);
            this.tabPage1.Controls.Add(this.btnPsychic);
            this.tabPage1.Controls.Add(this.btnFlying);
            this.tabPage1.Controls.Add(this.btnPoison);
            this.tabPage1.Controls.Add(this.btnBug);
            this.tabPage1.Controls.Add(this.btnDark);
            this.tabPage1.Controls.Add(this.btnIce);
            this.tabPage1.Controls.Add(this.btnFairy);
            this.tabPage1.Controls.Add(this.btnDragon);
            this.tabPage1.Controls.Add(this.btnGhost);
            this.tabPage1.Controls.Add(this.btnFighting);
            this.tabPage1.Controls.Add(this.btnNormal);
            this.tabPage1.Controls.Add(this.btnSteel);
            this.tabPage1.Controls.Add(this.btnGround);
            this.tabPage1.Controls.Add(this.btnRock);
            this.tabPage1.Controls.Add(this.btnGrass);
            this.tabPage1.Controls.Add(this.btnWater);
            this.tabPage1.Controls.Add(this.btnFire);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(406, 564);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Choose";
            // 
            // btnElectric
            // 
            this.btnElectric.BackColor = System.Drawing.Color.Gold;
            this.btnElectric.Location = new System.Drawing.Point(243, 411);
            this.btnElectric.Name = "btnElectric";
            this.btnElectric.Size = new System.Drawing.Size(75, 75);
            this.btnElectric.TabIndex = 17;
            this.btnElectric.Text = "Electric";
            this.btnElectric.UseVisualStyleBackColor = false;
            // 
            // btnPsychic
            // 
            this.btnPsychic.BackColor = System.Drawing.Color.Fuchsia;
            this.btnPsychic.Location = new System.Drawing.Point(162, 411);
            this.btnPsychic.Name = "btnPsychic";
            this.btnPsychic.Size = new System.Drawing.Size(75, 75);
            this.btnPsychic.TabIndex = 16;
            this.btnPsychic.Text = "Psychic";
            this.btnPsychic.UseVisualStyleBackColor = false;
            // 
            // btnFlying
            // 
            this.btnFlying.BackColor = System.Drawing.Color.PowderBlue;
            this.btnFlying.Location = new System.Drawing.Point(81, 411);
            this.btnFlying.Name = "btnFlying";
            this.btnFlying.Size = new System.Drawing.Size(75, 75);
            this.btnFlying.TabIndex = 15;
            this.btnFlying.Text = "Flying";
            this.btnFlying.UseVisualStyleBackColor = false;
            // 
            // btnPoison
            // 
            this.btnPoison.BackColor = System.Drawing.Color.DarkMagenta;
            this.btnPoison.Location = new System.Drawing.Point(243, 330);
            this.btnPoison.Name = "btnPoison";
            this.btnPoison.Size = new System.Drawing.Size(75, 75);
            this.btnPoison.TabIndex = 14;
            this.btnPoison.Text = "Poison";
            this.btnPoison.UseVisualStyleBackColor = false;
            // 
            // btnBug
            // 
            this.btnBug.BackColor = System.Drawing.Color.GreenYellow;
            this.btnBug.Location = new System.Drawing.Point(162, 330);
            this.btnBug.Name = "btnBug";
            this.btnBug.Size = new System.Drawing.Size(75, 75);
            this.btnBug.TabIndex = 13;
            this.btnBug.Text = "Bug";
            this.btnBug.UseVisualStyleBackColor = false;
            // 
            // btnDark
            // 
            this.btnDark.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnDark.Location = new System.Drawing.Point(81, 330);
            this.btnDark.Name = "btnDark";
            this.btnDark.Size = new System.Drawing.Size(75, 75);
            this.btnDark.TabIndex = 12;
            this.btnDark.Text = "Dark";
            this.btnDark.UseVisualStyleBackColor = false;
            // 
            // btnIce
            // 
            this.btnIce.BackColor = System.Drawing.Color.Turquoise;
            this.btnIce.Location = new System.Drawing.Point(243, 249);
            this.btnIce.Name = "btnIce";
            this.btnIce.Size = new System.Drawing.Size(75, 75);
            this.btnIce.TabIndex = 11;
            this.btnIce.Text = "Ice";
            this.btnIce.UseVisualStyleBackColor = false;
            // 
            // btnFairy
            // 
            this.btnFairy.BackColor = System.Drawing.Color.Plum;
            this.btnFairy.Location = new System.Drawing.Point(162, 249);
            this.btnFairy.Name = "btnFairy";
            this.btnFairy.Size = new System.Drawing.Size(75, 75);
            this.btnFairy.TabIndex = 10;
            this.btnFairy.Text = "Fairy";
            this.btnFairy.UseVisualStyleBackColor = false;
            // 
            // btnDragon
            // 
            this.btnDragon.BackColor = System.Drawing.Color.DarkViolet;
            this.btnDragon.Location = new System.Drawing.Point(81, 249);
            this.btnDragon.Name = "btnDragon";
            this.btnDragon.Size = new System.Drawing.Size(75, 75);
            this.btnDragon.TabIndex = 9;
            this.btnDragon.Text = "Dragon";
            this.btnDragon.UseVisualStyleBackColor = false;
            // 
            // btnGhost
            // 
            this.btnGhost.BackColor = System.Drawing.Color.SlateBlue;
            this.btnGhost.Location = new System.Drawing.Point(243, 168);
            this.btnGhost.Name = "btnGhost";
            this.btnGhost.Size = new System.Drawing.Size(75, 75);
            this.btnGhost.TabIndex = 8;
            this.btnGhost.Text = "Ghost";
            this.btnGhost.UseVisualStyleBackColor = false;
            // 
            // btnFighting
            // 
            this.btnFighting.BackColor = System.Drawing.Color.DarkRed;
            this.btnFighting.Location = new System.Drawing.Point(162, 168);
            this.btnFighting.Name = "btnFighting";
            this.btnFighting.Size = new System.Drawing.Size(75, 75);
            this.btnFighting.TabIndex = 7;
            this.btnFighting.Text = "Fighting";
            this.btnFighting.UseVisualStyleBackColor = false;
            // 
            // btnNormal
            // 
            this.btnNormal.BackColor = System.Drawing.Color.White;
            this.btnNormal.Location = new System.Drawing.Point(81, 168);
            this.btnNormal.Name = "btnNormal";
            this.btnNormal.Size = new System.Drawing.Size(75, 75);
            this.btnNormal.TabIndex = 6;
            this.btnNormal.Text = "Normal";
            this.btnNormal.UseVisualStyleBackColor = false;
            // 
            // btnSteel
            // 
            this.btnSteel.BackColor = System.Drawing.Color.Silver;
            this.btnSteel.Location = new System.Drawing.Point(243, 87);
            this.btnSteel.Name = "btnSteel";
            this.btnSteel.Size = new System.Drawing.Size(75, 75);
            this.btnSteel.TabIndex = 5;
            this.btnSteel.Text = "Steel";
            this.btnSteel.UseVisualStyleBackColor = false;
            // 
            // btnGround
            // 
            this.btnGround.BackColor = System.Drawing.Color.SaddleBrown;
            this.btnGround.Location = new System.Drawing.Point(162, 87);
            this.btnGround.Name = "btnGround";
            this.btnGround.Size = new System.Drawing.Size(75, 75);
            this.btnGround.TabIndex = 4;
            this.btnGround.Text = "Ground";
            this.btnGround.UseVisualStyleBackColor = false;
            // 
            // btnRock
            // 
            this.btnRock.BackColor = System.Drawing.Color.SandyBrown;
            this.btnRock.Location = new System.Drawing.Point(81, 87);
            this.btnRock.Name = "btnRock";
            this.btnRock.Size = new System.Drawing.Size(75, 75);
            this.btnRock.TabIndex = 3;
            this.btnRock.Text = "Rock";
            this.btnRock.UseVisualStyleBackColor = false;
            // 
            // btnGrass
            // 
            this.btnGrass.BackColor = System.Drawing.Color.Green;
            this.btnGrass.Location = new System.Drawing.Point(243, 6);
            this.btnGrass.Name = "btnGrass";
            this.btnGrass.Size = new System.Drawing.Size(75, 75);
            this.btnGrass.TabIndex = 2;
            this.btnGrass.Text = "Grass";
            this.btnGrass.UseVisualStyleBackColor = false;
            // 
            // btnWater
            // 
            this.btnWater.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnWater.Location = new System.Drawing.Point(162, 6);
            this.btnWater.Name = "btnWater";
            this.btnWater.Size = new System.Drawing.Size(75, 75);
            this.btnWater.TabIndex = 1;
            this.btnWater.Text = "Water";
            this.btnWater.UseVisualStyleBackColor = false;
            // 
            // btnFire
            // 
            this.btnFire.BackColor = System.Drawing.Color.OrangeRed;
            this.btnFire.Location = new System.Drawing.Point(81, 6);
            this.btnFire.Name = "btnFire";
            this.btnFire.Size = new System.Drawing.Size(75, 75);
            this.btnFire.TabIndex = 0;
            this.btnFire.Text = "Fire";
            this.btnFire.UseVisualStyleBackColor = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lblResults);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(406, 493);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "View Defences";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblResults
            // 
            this.lblResults.AutoSize = true;
            this.lblResults.Font = new System.Drawing.Font("Moire", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResults.Location = new System.Drawing.Point(3, 0);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(57, 19);
            this.lblResults.TabIndex = 1;
            this.lblResults.Text = "______";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lblOffences2);
            this.tabPage3.Controls.Add(this.lblOffences);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(406, 493);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "View Offences";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lblOffences2
            // 
            this.lblOffences2.AutoSize = true;
            this.lblOffences2.Font = new System.Drawing.Font("Moire", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOffences2.Location = new System.Drawing.Point(201, 0);
            this.lblOffences2.Name = "lblOffences2";
            this.lblOffences2.Size = new System.Drawing.Size(49, 15);
            this.lblOffences2.TabIndex = 3;
            this.lblOffences2.Text = "______";
            // 
            // lblOffences
            // 
            this.lblOffences.AutoSize = true;
            this.lblOffences.Font = new System.Drawing.Font("Moire", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOffences.Location = new System.Drawing.Point(3, 0);
            this.lblOffences.Name = "lblOffences";
            this.lblOffences.Size = new System.Drawing.Size(49, 15);
            this.lblOffences.TabIndex = 2;
            this.lblOffences.Text = "______";
            // 
            // lblSelected
            // 
            this.lblSelected.AutoSize = true;
            this.lblSelected.Font = new System.Drawing.Font("Moire", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelected.Location = new System.Drawing.Point(12, 606);
            this.lblSelected.Name = "lblSelected";
            this.lblSelected.Size = new System.Drawing.Size(242, 19);
            this.lblSelected.TabIndex = 2;
            this.lblSelected.Text = "PLEASE CHOOSE 1-2 TYPES";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(417, 634);
            this.Controls.Add(this.lblSelected);
            this.Controls.Add(this.tabController);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PokeType";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tabController.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabController;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnElectric;
        private System.Windows.Forms.Button btnPsychic;
        private System.Windows.Forms.Button btnFlying;
        private System.Windows.Forms.Button btnPoison;
        private System.Windows.Forms.Button btnBug;
        private System.Windows.Forms.Button btnDark;
        private System.Windows.Forms.Button btnIce;
        private System.Windows.Forms.Button btnFairy;
        private System.Windows.Forms.Button btnDragon;
        private System.Windows.Forms.Button btnGhost;
        private System.Windows.Forms.Button btnFighting;
        private System.Windows.Forms.Button btnNormal;
        private System.Windows.Forms.Button btnSteel;
        private System.Windows.Forms.Button btnGround;
        private System.Windows.Forms.Button btnRock;
        private System.Windows.Forms.Button btnGrass;
        private System.Windows.Forms.Button btnWater;
        private System.Windows.Forms.Button btnFire;
        private System.Windows.Forms.Label lblResults;
        private System.Windows.Forms.Label lblSelected;
        private System.Windows.Forms.Label lblOffences;
        private System.Windows.Forms.Label lblOffences2;
    }
}

