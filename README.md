This is a Windows form application created through VisualStudio. PokeType is a tool for helping players of the game "Pokemon" make strategically sound moves.

Briefly, Pokemon is an Attacker-Defender kind of game where a creature can be one of 18 different types. Varying types of creatures will have stronger or weaker attacks against other creatures. For example, WATER types beat FIRE types. With 18 different types, the game offers a multitude of combinations often difficult to remember. This tool assists players by allowing them to select two types and displaying the determined weaknesses and strengths of those types.

The important source code is in the directory /PokeType/Code/

To run, execute /PokeType/bin/Debug/PokeType.exe
Note: You require the .NET Framework installed on your machine.